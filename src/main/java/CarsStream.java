import java.io.DataOutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;
import java.util.Random;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

public class CarsStream {

	private static final String SPRING_XD_STREAM_URL = "http://localhost:8091";

	private Random random = new Random();
	private List<CarBrand> carBrands;

	public static void main(String[] args) throws Exception {
		CarsStream carsStream = new CarsStream();
		carsStream.loadData();
		for (int i = 0; i < 100; i++) {
			carsStream.postRandomData();
			Thread.sleep(20);
		}
	}

	private void postRandomData() throws Exception {
		CarBrand carBrand = carBrands.get(random.nextInt(carBrands.size()));
		Car car = carBrand.getCars().get(random.nextInt(carBrand.getCars().size()));
		HttpURLConnection xdConnection = (HttpURLConnection) new URL(SPRING_XD_STREAM_URL).openConnection();
		xdConnection.setRequestMethod("POST");
		xdConnection.setDoOutput(true);
		String postData = "{\"brand\": \"" + carBrand.getName() + "\", \"model\": \"" + car.getModel() + "\"}";
		System.out.println(postData);
		DataOutputStream wr = new DataOutputStream(xdConnection.getOutputStream());
		wr.writeBytes(postData);
		wr.flush();
		wr.close();
		xdConnection.getResponseCode();
	}

	public void loadData() throws Exception {
		ObjectMapper mapper = new ObjectMapper();
		carBrands = mapper.readValue(getClass().getResourceAsStream("cars.json"), new TypeReference<List<CarBrand>>() {
		});
	}
}
